# Minicursos - Programação Paralela

Exercícios e soluções dos minicursos:

- **Programação Paralela em Memória Compartilhada e Avaliação de Desempenho com Contadores de Hardware**
ERAD/RS 2020 (Santa Maria - RS)

- **Programação Paralela e Vetorial Avançada**
Escola Sdumont 2020 (Petrópolis - RJ)

- **Introdução à Programação Paralela e Vetorial**
Escola Sdumont 2020 (Petrópolis - RJ)

- **Programação Paralela e Vetorial em Memória Compartilhada e Distribuı́da**
ERAD/RS 2019 (Três de Maio - RS)

- **Introdução à Programação Paralela e Vetorial em Arquiteturas Intel Xeon Phi**
Escola Sdumont 2019 (Petrópolis - RJ)  

- **Intel Modern Code: Introdução à Programação Vetorial e Paralela para o Processador Intel Xeon Phi Knights Landing**
ERAD/RS 2018 (Porto Alegre - RS)

- **Introdução à Programação Paralela e Vetorial em Arquiteturas Intel Xeon Phi**
Escola Sdumont 2018 (Petrópolis - RJ)

- **Intel Modern Code: Programação Paralela e Vetorial AVX para o Processador Intel Xeon Phi Knights Landing**
WSCAD 2017 (Campinas - SP)

- **Intel Modern Code: Programação Vetorial e Paralela em Arquiteturas Intel Xeon e Intel Xeon Phi**
ERAD/RS 2017 (Ijuí - RS)
